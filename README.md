## 프로젝트명
```
Military Next - 다가오는 국방개혁 2.0 맞추어 국군장병이 필요로 하는 기능들을 가진 종합서비스앱.
```
## 개요
```
국방개혁 2.0이 도래하면서 전 국군 장병이 핸드폰을 사용하게 될날이 머지 않아 다가오고 있다.
그렇지만 기존에 핸드폰을 사용하지 않았던 군인들을 위한 어플리케이션 서비스가 없다.
그래서 국군 장병들이 필요로 하는 기능을 모아 종합 서비스 어플리케이션을 제작하였다.
```
## 구현기능

## 1. 인증
```
Firebase와 연동하여 군번 인증으로 회원가입 및 로그인을 할수 있도록 구현하였다.
```
#### 주요기능
- 회원가입
- 로그인

#### 페이지구성
![](./images/auth-merge.png)


## 2. 장터마당(합리적인 소비)
```
군인들이 공용으로 수요가 있는 제품(단백질 보충제, 도서, mp3플레이어등)은 웹링크와 연동하여 공동구매를 할수 있는 기능과
전역에 임박한 장병들의 군대 용품을 판매할 수 있고 신병들은 그로인해 싸게 필요한 물품을 구하기 쉬워짐
군번 인증 기반이라 사기의 위험성이 적음
```

#### 주요기능
- 공동 구매(클릭시 구매 웹페이지로이동)
- 중고 물품 전체보기(클릭시 상세보기로 이동) 
- 중고 물품 상세보기
- 물품 팔기

#### 페이지구성
![](./images/store-merge.png)

## 3. 도서마당(군인 독서 장려)
```
알라딘에서 가져온 베스트 셀러 정보를 보여주고 바로 구매를 할수 있게 웹링크
를 연결하게 하였다. 그외에도 다른 장병들의 도서 후기와 다독자 랭킹기능을 구현해 군 장병들의
독서 습관을 장려시킨다.
```

#### 주요기능
- 베스트셀러 순위열람 및 알라딘 구매링크 연결을 통한 해당상품 바로구매 가능
- 후기(책들을 읽은 군인들의 후기를 보면서 자신이 볼만할 책을 찾을 수 있음)
- 다독 랭킹(많이 도서를 읽은 순서대로 랭킹을 나열함.

#### 페이지구성
![](./images/book_merge.png)

## 4. 커뮤니티
```
블라인드, 대나무숲같은 익명 커뮤니티가 많은 순기능(고민상담, 정보획득, 부조리 관습 제거)을 가져오면서 
특정 그룹에 대한 익명커뮤니티에 필요성또한 커지고 있다. 군부대 내에 커뮤니티를 생성하면서 위와같은 순기능을 누림과 동시에
SNS 사용욕구를 군대 내에서 풀게하면서 사회적으로 SNS를 오용에 대한 군기강 해이 및 군인에 대한 사회적 인식 손상방지를 이뤄낼수 있다.
```

#### 주요기능
- 타임라인 보기
- 타임라인 쓰기
- 공지사항 보기(군인에게 필요한 정보를 내용과 함께 링크도 전달하여 클릭시 해당 링크로 이동가능)
- 내가쓴글 보기(타임라인에서 내가 쓴글을 바로 볼수 있음)

#### 페이지구성
![](./images/community-merge.png)


## 5. 부대마당
```
군부대 내 스터디를 장려하기 위한 모임기능과 군대 공지사항을 폰으로 확인하게 쉽게 만드는 공지사항기능
그리고 오늘 내 부대일정을 알려주는 부대 일정 리스트 기능이 구현되어있다. 또한 푸쉬알람과의 연동을 통해
오늘날짜 기준으로 나의 일정을 잊지않고 확인할 수 있다.
```

#### 주요기능
- 부대 공지사항 열람(작성된 부대 공지사항을 폰으로 수시로 확인할수 있어서 부대 전파사항을 효과적으로 확인가능)
- 나의 부대 일정 열람(서버에서 그 날에 맞는 일정을 불러와서 푸쉬알람을 띄워줌으로써 자신의 역할을 숙지가능)
- 부대 내 그룹기능

#### 페이지구성
![](./images/troop_merge.png)


## 6. 그 외 기타화면

- 메뉴 선택, 푸쉬알람, 스픞래시화면

#### 페이지구성
![](./images/etc_merge.png)


### 사용 라이브러리
```
'com.google.firebase:firebase-core:16.0.4'
'com.google.firebase:firebase-database:16.0.3'
'com.google.firebase:firebase-messaging:17.3.4'
'com.google.firebase:firebase-storage:16.0.3'
'com.android.support:recyclerview-v7:28.0.0'
'com.android.support:design:28.0.0'
'com.roughike:bottom-bar:1.4.0.1'
'com.squareup.retrofit2:retrofit:2.4.0'
'com.github.bumptech.glide:glide:4.8.0'
'com.android.support:appcompat-v7:28.0.0'
'com.android.support.constraint:constraint-layout:1.1.3'
'com.android.support:support-v4:28.0.0'

```


### License
```
MIT License

Copyright (c) 2018 jo youngje site: https://www.github.com/siosio34

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

